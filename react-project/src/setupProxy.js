/*
 * @Descripttion: your project
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-08-15 19:52:29
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-07 21:08:13
 */
const proxy = require('http-proxy-middleware')
// console.log(process.env.REACT_APP_BASE_URL)
let BaseUrl=process.env.REACT_APP_BASE_URL
module.exports = function(app) {
  app.use(
    proxy.createProxyMiddleware('/user/', {  //`api`是需要转发的请求 
      target: BaseUrl,  // 这里是接口服务器地址
      changeOrigin: true,
      // pathRewrite: {'^/user': ''}
    })
  )
  app.use(
    proxy.createProxyMiddleware('/phone/', {  //`api`是需要转发的请求 
      target: BaseUrl,  // 这里是接口服务器地址
      changeOrigin: true,
      // pathRewrite: {'^/user': ''}
    })
  )
}