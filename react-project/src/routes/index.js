/*
 * @Descripttion: 路由配置
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-08-05 12:31:21
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-11 18:39:49
 */
import {HomeOutlined} from '@ant-design/icons';
//  登陆页面
import Login from '../views/login/login'
//404页面
import NotFound from '../views/notFound/Index'
// 主页
import Admin from '../App'
import PhoneManage from '../views/phoneManage/PhoneManage'
import ReaderManage from '../views/readerManage/ReaderManage';
import UserManage from '../views/userManage/UserManage';
import Findpwd from '../views/userManage/findPwd';
// 登录路由
const loginRouter=[
    {
        path:'/login',
        component:Login,
        name:'login',
        title:'登陆页面',
        icon:HomeOutlined
    },
    {
        path:'/404',
        component:NotFound,
        name:'404',
        title:'错误页面',
        icon:HomeOutlined
    }
]

const findPwdRouter=[
    {
        path:'/findpwd',
        component:Findpwd,
        name:'findpwd',
        title:'找回密码',
    }
]

// 管理后台路由
const adminRouter=[
    // {
    //     path:'/admin/userManage',
    //     component:UserManage,
    //     title:'用户列表',
    //     name:'userManage',
    //     exact:true,
    //     icon:'icon-user_pre'
    // }
    {
        path:'/admin/phoneManage',
        component:Admin,
        title:'手机管理',
        name:'/admin/phoneManage',
        icon:HomeOutlined,
        children:[
            {
                path:'/admin/phoneList',
                component:PhoneManage,
                title:'手机列表',
                name:'/admin/phoneList',
                icon:HomeOutlined
            }
        ]
    },
    {
        path:'/admin/talkManage',
        component:Admin,
        title:'评论管理',
        name:'/admin/talkManage',
        icon:HomeOutlined,
        children:[
            {
                path:'/admin/talkList',
                component:ReaderManage,
                title:'评论列表',
                name:'/admin/talkList',
                icon:HomeOutlined
            }
        ]
    },
    {
        path:'/admin/userManage',
        component:UserManage,
        title:'用户管理',
        name:'/admin/userManage',
        icon:HomeOutlined,
    }
]

export{
    loginRouter,
    adminRouter,
    findPwdRouter
}