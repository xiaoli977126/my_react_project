import React,{useState} from 'react'
import { Input,Button,message } from 'antd';
import  styles from './findPwd.less';
import {findpwd} from '@/utils/interface.js'
function Findpwd(props) {
    const [emailAndphone,setEmailAndPhone]=useState('')//输入框内容
    const [flag,setFlag]=useState(false)//控制是否显示密码结果
    const [password,setPassword]=useState('')//密码赋值
    //找回密码
    function findpwdFunc(){
        let obj={
            emailAndphone
        }
        findpwd(obj).then(res=>{
            if(res.data.status===200){
                message.success(`${res.data.msg}`)
                setFlag(true)
                setPassword(res.data.password)
                setEmailAndPhone('')
            }else{
                message.error(`${res.data.msg}`)
            }
        })
    }
    //输入框
    function inputChange(e){
        setEmailAndPhone(e.target.value)
    }
    //返回登录页面
    function backLoginFunc(){
        props.history.goBack()
    }
    //聚焦的时候
    function focusFunc(){
        if(password){
            setFlag(false)
        }
    }
    //失焦的时候
    function blurFunc(){
        if(password){
            setFlag(true)
        }
    }
    return (
        <div className={styles['findpwd-container']}>
            <div className={styles['findpwd-container-outer']}>
                <div className={styles['findpwd-content']}>
                    <div>
                        <Input
                        placeholder="手机号码或邮箱"
                        onFocus={()=>{focusFunc()}}
                        value={emailAndphone}
                        onChange={inputChange}
                        onBlur={()=>{blurFunc()}}
                            />
                    </div>
                    <div className={styles['findpwd-btn']} onClick={()=>{findpwdFunc()}} >
                        <Button type="primary" disabled={!emailAndphone}>找回密码</Button>
                    </div>
                    <div className={styles['findpwd-btn']} onClick={()=>{backLoginFunc()}} >
                        <Button type="primary" >返回登陆页面</Button>
                    </div>
                </div>
                <div style={{display:flag?'block':'none',textAlign:'center'}}>
                    <span>你的密码是:{password}</span>
                </div>
            </div>
        </div>
    )
}

export default Findpwd
