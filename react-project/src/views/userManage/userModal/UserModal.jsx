import React,{useState} from 'react'
import { Modal,Form, Input,message,Radio} from 'antd';
import {addOrSave} from '@/utils/interface.js'

function UserModal(props) {
    const [form] = Form.useForm();
    const [isModalVisible, setIsModalVisible] = useState(false);//控制是否打开弹框
    const [title,setTitle]=useState('添加手机信息')
    const [id,setId]=useState('')
     const [rolename, setRolename] = React.useState('');//设置角色
    // console.log('refs:',props)

    const onChange = e => {
        console.log('radio checked', e.target.value);
        setRolename(e.target.value);
    };
    props.refs.current={
       addUserModal :()=>{//打开弹框
            setTitle('添加用户信息')
            setIsModalVisible(true);
        },
        editUserModal :(record)=>{//打开弹框
            setTitle('修改用户信息')
            setIsModalVisible(true);
            form.setFieldsValue({
                username:record.username,
                telephone:record.telephone,
                email:record.email,
                password:record.password,
                comfirmPwd:record.password,
                rolename:record.rolename?record.rolename:''
            })
            setId(record._id)
        }
    }
    const handleOk = () => {//确定按钮
        form.submit()
    };
    
    const handleCancel = () => {//取消按钮
        setIsModalVisible(false);
    };
    
    const onFinish = (values) => {//表单验证通过
        var obj={}
        if(id){
            obj={
                username:values.username,
                telephone:values.telephone,
                password:values.password,
                email:values.email,
                rolename:values.rolename,
                id:id
            }
        }else{
            obj={
                username:values.username,
                telephone:values.telephone,
                password:values.password,
                email:values.email,
                rolename:values.rolename,
            }
        }
      
        addOrSave(obj).then(res=>{
            if(res.data.status===200){
                message.success(`${res.data.msg}`);
                props.refresh()
                setIsModalVisible(false)
            }else{
                message.error(`${res.data.msg}`);
                setIsModalVisible(true)
            }
        })
    };
    
    const onFinishFailed = (errorInfo) => {//表单验证没通过
        console.log('Failed:', errorInfo);
        setIsModalVisible(true);

    };

    // 弹框关闭的回调
    function closeModal(){
        form.resetFields()
        setTitle('添加手机信息')
        setIsModalVisible(false)
        setId('')
    }
    return (
        <div>
            <Modal 
            maskClosable={false}
            afterClose={closeModal}
            title={title} visible={isModalVisible} 
            onOk={handleOk} 
            onCancel={handleCancel}
            okText='提交'
            cancelText='取消'
            >
                <Form
                form={form}
                name="basic"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 16 }}
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="用户名"
                        name="username"
                        rules={
                            [
                                { required: true, message: '请输入用户名!' }
                            ]
                        }
                    >
                        <Input  />
                    </Form.Item>

                    <Form.Item
                        label="邮箱"
                        name="email"
                        rules={
                            [
                                { required: true, message: '请输入email' },
                                {type:'email',message:'请输入正确的邮箱地址'}
                            ]
                        }
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="电话号码"
                        name="telephone"
                        rules={
                            [
                                { required: true, message: '请输入telephone' }
                            ]
                        }
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="密码"
                        name="password"
                        rules={
                            [
                                { required: true, message: '请输入密码' },
                                
                            ]
                        }
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        label="确认密码"
                        name="comfirmPwd"
                        dependencies={['password']}
                        rules={
                            [
                                { required: true, message: '确认密码' },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                      if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                      }
                        
                                      return Promise.reject(new Error('与新密码不配对'));
                                    },
                                }),
                            ]
                            }
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        label="角色"
                        name="rolename"
                        rules={[{ required: true, message: '请输入角色!' }]}
                    >
                        <Radio.Group onChange={onChange} value={rolename}>
                            <Radio value={'普通用户'}>普通用户</Radio>
                            <Radio value={'管理员'}>管理员</Radio>
                        </Radio.Group>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}

export default UserModal
