import React,{useState,useEffect,useRef} from 'react'
import {Table,Pagination,Button,message,Popconfirm} from 'antd'
import {queryPage,delUser} from '@/utils/interface.js'
import {DeleteOutlined,EditOutlined,PlusOutlined} from '@ant-design/icons';
import styles from './userManage.less'
import UserModal from './userModal/UserModal';
function UserManage() {
    /*********************数据初始化************************/
    //表格标题
    const columns = [
        {
          title: 'ID',
          dataIndex: '_id',
          key: '_id',
        },
        {
          title: '用户名',
          dataIndex: 'username',
          key: 'username',
        },
        {
          title: '邮箱',
          dataIndex: 'email',
          key: 'email',
        },
        {
            title: '电话号码',
            dataIndex: 'telephone',
            key: 'telephone', 
        },
        {
            title: '操作',
            key: 'option',
            render:(text,record)=>{
                return(
                    <>
                        <Button type="primary" style={{marginRight:'10px'}} size='middle' onClick={()=>{editUserFunc(record)}}><EditOutlined /><span>修改</span></Button>
                        <Popconfirm
                            title="确定删除该用户信息?"
                            onConfirm={()=>{delUserFunc(record)}}
                            onCancel={cancel}
                            okText="Yes"
                            cancelText="No"
                        >
                            <Button type="primary"  size='middle' danger  ><DeleteOutlined  /><span>删除</span></Button>
                        </Popconfirm>
                    </>
                )
            }
        },
    ];
      //表格数据
    const dataSource = [];
    const [tableData,setTableData]=useState(dataSource)//表格数据赋值
    const [pageSize,setPageSize]=useState(5)//分页大小
    const [current,setCurrent]=useState(1)//当前页
    const [total,setTotal]=useState(100)//总数
    const pageSizeOptions=['5','10','20','30']//选择页码大小
    const userRef=useRef()



 /******************************函数初始化***************************** */
    //请求接口获取table数据
    useEffect(()=>{
        let obj={
            pageSize,
            current
        }
        getList(obj)
        console.log('pageSize:',pageSize,'current:',current);
    },[pageSize,current])
    //获取表格数据
    function getList(obj){
        queryPage(obj).then(res=>{
            if(res.data.status===200){
                setTableData(res.data.list)
                setTotal(res.data.total)
            }
        }).catch((err)=>{
            return
        })
    }
    //分页按钮change
    function onChange(current,pageSize) {
        console.log(current,pageSize);
        var obj={
          current:current,
          pageSize:pageSize
        }
        getList(obj)
        setCurrent(current)
        setPageSize(pageSize)
    }
    //show总数
    function showTotal(total) {
        return `总数 ${total} `;
    }
    //添加用户
    function addUserFunc(){
        userRef.current.addUserModal()
    }
    //修改用户
    function editUserFunc(record){
        userRef.current.editUserModal(record)
    }
    //删除用户
    function delUserFunc(record){
        let obj={
            id:record._id
        }
        let num=0
        delUser(obj).then(res=>{
            if(res.data.status===200){
                if(total%pageSize===1&&Math.ceil(total/pageSize)===current){
                    num=current-1
                    getList({current:num,pageSize})
                    setCurrent(num)
                  }else{
                    getList({current,pageSize})
                  }
                  message.destroy()
                  message.success(`${res.data.msg}`)
            }else{
                message.destroy()
                message.success(`${res.data.msg}`)
            }
        })
    }
    //添加或修改回调
    function refresh(){
        var objRefresh={
          current,
          pageSize
        }
        getList(objRefresh)
    }
    //删除的取消按钮
    function cancel(e) {
        message.success('已取消删除');
    }
    return (
        <div className={styles['user-content']}>
            <UserModal refs={userRef} refresh={()=>{refresh()}}></UserModal>
            <div className={styles.option}>
                <div>
                    <Button type="primary" style={{marginRight:'10px'}} onClick={()=>{addUserFunc()}} size='middle'><PlusOutlined /><span>添加</span></Button>
                </div>
            </div>
            <div className={styles.table}>
                <Table dataSource={tableData} rowKey='_id' columns={columns} pagination={false} />
            </div>
            <div className='pagination'>
                <Pagination
                    showQuickJumper
                    defaultCurrent={current}
                    total={total}
                    showTotal={showTotal}
                    pageSizeOptions={pageSizeOptions}
                    showSizeChanger
                    defaultPageSize={5}
                    onChange={onChange}
                    current={current}
                />
            </div>
        </div>
    )
}

export default UserManage
