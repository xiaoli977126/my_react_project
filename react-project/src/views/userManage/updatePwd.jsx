import React,{useState} from 'react'
import { Modal,Form, Input,message} from 'antd';
import './updatePwd.less'
import {editUserPwd} from '@/utils/interface.js'
import {removeToken} from '@/utils/auth.js'
import {withRouter} from 'react-router-dom'
function UpdateModal(props) {
    const [form] = Form.useForm();
    const [isModalVisible, setIsModalVisible] = useState(false);//控制是否打开弹框
    const title='修改密码'
    const [id,setId]=useState('')
    // console.log('refs:',props)
    props.pwdRef.current={
        editPwdModal :(userInfo)=>{//打开弹框
            setIsModalVisible(true)
            setId(userInfo.id)
            console.log('props:',props)
        }
    }
    const handleOk = () => {//确定按钮
        form.submit()
    };
    
    const handleCancel = () => {//取消按钮
        setIsModalVisible(false);
    };
    
    const onFinish = (values) => {//表单验证通过
        let obj={
            oldPwd:values.oldPwd,
            newPwd:values.newPwd,
            id:id
        }
        editUserPwd(obj).then(res=>{
            if(res.data.status===200){
                message.success(`${res.data.msg}`);
                setIsModalVisible(false)
                removeToken()
                props.history.push('/login')
            }else{
                message.error(`${res.data.msg}`);
                setIsModalVisible(true)
            }
        })
    };
    
    const onFinishFailed = (errorInfo) => {//表单验证没通过
        console.log('Failed:', errorInfo);
        setIsModalVisible(true);

    };

    // 弹框关闭的回调
    function closeModal(){
        form.resetFields()
    }
    return (
        <div>
            <Modal 
            maskClosable={false}
            afterClose={closeModal}
            title={title}
            visible={isModalVisible}
            onOk={handleOk}
            onCancel={handleCancel}
            okText='提交'
            cancelText='取消'
            >
                <Form
                form={form}
                name="basic"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 16 }}
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                >
                    <Form.Item
                        label="旧密码"
                        name="oldPwd"
                        hasFeedback
                        rules={[{ required: true, message: '请输入旧密码!' }]}
                    >
                        <Input.Password  />
                    </Form.Item>

                    <Form.Item
                        label="新密码"
                        name="newPwd"
                        hasFeedback
                        rules={
                            [
                                { required: true, message: '请输入新密码' },
                                
                            ]
                        }
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        label="确认密码"
                        name="comfirmPwd"
                        hasFeedback
                        dependencies={['newPwd']}
                        rules={
                            [
                                { required: true, message: '确认密码' },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                      if (!value || getFieldValue('newPwd') === value) {
                                        return Promise.resolve();
                                      }
                        
                                      return Promise.reject(new Error('与新密码不配对'));
                                    },
                                }),
                            ]
                            }
                    >
                        <Input.Password />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}

export default withRouter(UpdateModal)
