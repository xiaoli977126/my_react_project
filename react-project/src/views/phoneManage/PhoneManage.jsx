import React,{useState,useEffect,useRef} from 'react'
import {Table,Button,Pagination, message ,Popconfirm} from 'antd'
import {DeleteOutlined,EditOutlined,PlusOutlined} from '@ant-design/icons';
import styles from './phoneManage.less'
import Mymodal from './modal/modal';
import {phoneQueryPages,phoneDel} from '@/utils/interface.js'
function UserManage(props) {
  /*********************变量初始化********************** */
   // 表格标题
    const columns = [
      {
        title: 'ID',
        dataIndex: '_id',
        key: '_id',
      },
      {
        title: '手机名称',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: '价格',
        dataIndex: 'price',
        key: 'price',
      },
      {
          title: '操作',
          key: 'option',
          render:(text,record)=>{
              return(
                  <>
                      <Button type="primary" style={{marginRight:'10px'}} size='middle' onClick={()=>{editPhone(record)}}><EditOutlined /><span>修改</span></Button>
                      <Popconfirm
                        title="确认删除该手机信息吗?"
                        onConfirm={()=>{delPhone(record)}}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                      >
                      <Button type="primary"  size='middle' danger ><DeleteOutlined  /><span>删除</span></Button>
                      </Popconfirm>
                  </>
              )
          }
      },
    ];
    //表格数据
    const data = [
      {
        key: '1',
        _id: '11',
        name: '华为p50',
        price: 6499,
      },
      {
        key: '2',
        _id: '22',
        name: '华为mate40',
        price: '6999',
      },
    ]
    const childRef=useRef()//获取ref
    const [current,setCurrent]=useState(1)//获取当前页
    const [pageSize,setPageSize]=useState(5)//获取页码大小
    const [total,setTotal]=useState(100)//总数
    const pageSizeOptions=['5','10','20','30']//选择页码大小
    const[dataSource,setDataSource]=useState(data)//table数据初始化
    /***********************函数初始化************************* */
    //添加手机信息
    function addPhoneFunc(){
      childRef.current.addPhoneModal()
    }
    //修改手机信息
    function editPhone(record){
      childRef.current.editPhoneModal(record)
    }
    //获取表格数据
    function getList(obj){
      phoneQueryPages(obj).then(res=>{
        if(res.data.status===200){
          setDataSource(res.data.list)
          setTotal(res.data.total)
        }
      })
    }
    //有更改操作需要重新刷新数据
    function refresh(){
      var objRefresh={
        current,
        pageSize
      }
      getList(objRefresh)
    }
    //删除手机信息
    function delPhone(record){
      let obj={
        id:record._id
      }
      phoneDel(obj).then(res=>{
        var num
        if(res.data.status===200){
          if(total%pageSize===1&&Math.ceil(total/pageSize)===current){
            num=current-1
            getList({current:num,pageSize})
            setCurrent(num)
          }else{
            getList({current,pageSize})
          }
          message.destroy()
          message.success('删除成功')
        }else{
          message.error('删除失败')
        }
      })
    }
    //删除的取消按钮
    function cancel(e) {
      message.success('已取消删除');
    }
    //上来就获取table数据
    useEffect(()=>{
      let obj={
        current:current,
        pageSize:pageSize
      }
      getList(obj)
    },[current,pageSize])

    //总个数提示
    function showTotal(total) {
      return `总数 ${total} `;
    }
    //pageSize大小
    // function onShowSizeChange(current, pageSize) {
    //   var obj={
    //     current:current,
    //     pageSize:pageSize
    //   }
    //   getList(obj)
    //   setPageSize(pageSize)
    // }
    //当前页
    function onChange(current,pageSize) {
      console.log(current,pageSize);
      var obj={
        current:current,
        pageSize:pageSize
      }
      getList(obj)
      setCurrent(current)
      setPageSize(pageSize)
    }
    return (
        <div className={styles[['phone-content']]}>
            <Mymodal refs={childRef} refresh={()=>{refresh()}}></Mymodal>
            <div className={styles.option}>
                <div>
                    <Button type="primary" style={{marginRight:'10px'}} onClick={()=>{addPhoneFunc()}} size='middle'><PlusOutlined /><span>添加</span></Button>
                </div>
            </div>
            <div className={styles.table}>
                <Table pagination={false} rowKey='_id' dataSource={dataSource} columns={columns} />
            </div>
            <div className="pagination">
              <Pagination
                showQuickJumper
                defaultCurrent={current}
                total={total}
                showTotal={showTotal}
                pageSizeOptions={pageSizeOptions}
                showSizeChanger
                defaultPageSize={5}
                onChange={onChange}
                current={current}
              />
            </div>
        </div>
    )
}

export default UserManage
