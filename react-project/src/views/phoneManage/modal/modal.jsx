import React,{useState} from 'react'
import { Modal,Form, Input,message,Row, Col} from 'antd';
import './modal.less'
import {phoneAddOrSave} from '@/utils/interface.js'

function Mymodal(props) {
    const [form] = Form.useForm();
    const [isModalVisible, setIsModalVisible] = useState(false);//控制是否打开弹框
    const [title,setTitle]=useState('添加手机信息')
    const [id,setId]=useState('')
    // console.log('refs:',props)
    props.refs.current={
       addPhoneModal :()=>{//打开弹框
            setTitle('添加手机信息')
            setIsModalVisible(true);
            console.log('id',id);
        },
        editPhoneModal :(record)=>{//打开弹框
            setTitle('修改手机信息')
            setIsModalVisible(true);
            form.setFieldsValue({
                name:record.name,
                price:record.price
            })
            setId(record._id)
        }
    }
    const handleOk = () => {//确定按钮
        form.submit()
    };
    
    const handleCancel = () => {//取消按钮
        setIsModalVisible(false);
    };
    
    const onFinish = (values) => {//表单验证通过
        var obj={}
        if(id){
            obj={
                name:values.name,
                price:values.price,
                id:id
            }
        }else{
            obj={
                name:values.name,
                price:values.price
            }
        }
      
        phoneAddOrSave(obj).then(res=>{
            if(res.data.status===200){
                message.success(`${res.data.msg}`);
                props.refresh()
                setIsModalVisible(false)
            }else{
                message.error(`${res.data.msg}`);
                setIsModalVisible(true)
            }
        })
    };
    
    const onFinishFailed = (errorInfo) => {//表单验证没通过
        console.log('Failed:', errorInfo);
        setIsModalVisible(true);

    };

    // 弹框关闭的回调
    function closeModal(){
        form.resetFields()
        setId('')
    }
    return (
        <div>
            <Modal 
            maskClosable={false}
            afterClose={closeModal}
            title={title} visible={isModalVisible} 
            onOk={handleOk} 
            onCancel={handleCancel}
            okText='提交'
            cancelText='取消'
            width={800}
            >
                <Form
                form={form}
                name="basic"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                initialValues={{ remember: true }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                >
                    <Row gutter='36'>
                        <Col span='12'>
                            <Form.Item
                                label="手机名称"
                                name="name"
                                rules={[{ required: true, message: '请输入手机名称!' }]}
                            >
                                <Input  />
                            </Form.Item>
                        </Col>

                        <Col span='12'>
                            <Form.Item
                                label="价格"
                                name="price"
                                rules={[{ required: true, message: '请输入价格' }]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </div>
    )
}

export default Mymodal
