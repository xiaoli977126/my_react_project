import React from 'react'
import styles from './index.less';
import {Button} from 'antd'
function Index(props) {
    function backHomeFunc(){
        props.history.push('/admin/phoneList')
    }
    return (
        <div className={styles.container}>
            <div className={styles.btn} onClick={()=>{backHomeFunc()}}>
                <Button type='primary'>Back to home</Button>
            </div>
        </div>
    )
}

export default Index
