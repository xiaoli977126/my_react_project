/*
 * @Descripttion: 登录页面
 * @version: 1.0
 * @Author: 小李子
 * @Date: 2021-09-07 20:27:12
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-12 16:11:16
 */

import React from 'react'
import { Form, Input, Button,message,Radio } from 'antd';
import  styles from './login.less'
import {login} from '../../utils/interface'
import {setToken}from '@/utils/auth'
import {connect} from 'react-redux'
const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (data) => {
            dispatch({
                type:'USER_LOGIN',
                data
            })
        }
    }
}

function Index(props) {
    //设置角色
    const [rolename, setRolename] = React.useState('');

    //设置角色的change
    const onChange = e => {
        console.log('radio checked', e.target.value);
        setRolename(e.target.value);
    };

    //登录成功的提示
    const success = () => {
        message.success('登录成功');
    };
    // 提交成功
    const onFinish = async(values) => {
        console.log('Success:', values);
        let obj={
            username:values.username,
            password:values.password,
            rolename:values.rolename
        }
        let res=await login(obj)
        if(res.data.status===200){
            setToken(res.data.token)
            props.history.push('/admin')
            props.userLogin(res.data.params)
            success()
        }else{
            message.error(`${res.data.msg}`)
        }
        console.log(obj)
        props.history.push('/admin')

      };
    // 提交失败
      const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };

      //找回密码
      function findpwdFunc(){
        props.history.push('/findpwd')
      }
    return (
        <div className={styles['login-content']}>
            <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            className={styles['form-style']}
            labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 16,
              }}
            >
            <Form.Item  labelCol={{span:0}} wrapperCol={{span:0}}>
                <div className={styles['form-title']}>
                    <span>手机管理系统测试</span>
                </div>
            </Form.Item>
            <Form.Item
                label="用户名"
                name="username"
                rules={[{ required: true, message: '请输入用户名!' }]}
            >
                <Input placeholder='用户名' />
            </Form.Item>
        
            <Form.Item
                placeholder='密码'
                label="密码"
                name="password"
                rules={[{ required: true, message: '请输入密码!' }]}
            >
                <Input.Password placeholder='密码' />
            </Form.Item>
            <Form.Item
                label="角色"
                name="rolename"
                rules={[{ required: true, message: '请输入角色!' }]}
            >
                <Radio.Group onChange={onChange} value={rolename}>
                    <Radio value={'普通用户'}>普通用户</Radio>
                    <Radio value={'管理员'}>管理员</Radio>
                </Radio.Group>
            </Form.Item>
        
        
            <Form.Item  className={styles['form-btn']} labelCol={{span:0}} wrapperCol={{span:0}}>
                <Button type="primary" htmlType="submit">
                登录
                </Button>
            </Form.Item>
            <Form.Item  labelCol={{span:0}} wrapperCol={{span:0}}>
                <div className={styles['form-forgetpwd']} onClick={()=>{findpwdFunc()}}>
                    <span>忘记密码</span>
                </div>
            </Form.Item>
            </Form>
        </div>
      );
}

export default connect(null,mapDispatchToProps)(Index)
