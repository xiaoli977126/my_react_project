import React,{useState,useEffect,useRef} from 'react'
import { Layout, Menu, Breadcrumb,Modal, Dropdown, Button,Spin } from 'antd';
import { ExclamationCircleOutlined,CaretDownOutlined } from '@ant-design/icons';
// import { UserOutlined, LaptopOutlined, NotificationOutlined} from '@ant-design/icons';
import styles from './index.less'
import {adminRouter} from '../../routes/index'
import {withRouter,useLocation} from 'react-router-dom'
import {removeToken} from '@/utils/auth.js'
import {userInfo} from '@/utils/interface.js'
import UpdateModal from '../userManage/updatePwd';
import {connect} from 'react-redux'
const mapStateToProps = (state, ownProps) => {
    return {
        userData: state.userData,
        spinning:state.spinning
    }
}
function Frame(props) {
    // console.log('frame',props)
    let menuList=props.menuList
    let firstTitle=localStorage.getItem('firstTitle')
    let secondTitle=localStorage.getItem('secondTitle')
    let pathKey=localStorage.getItem('pathKey')
    let {pathname}=useLocation()
    const { SubMenu } = Menu;
    const { Header, Content, Sider } = Layout;
    const [userInfoData,setUserInfo]=useState({})
    const updatePwd=useRef()
    //用户的下拉菜单
    const menu = (
        <Menu>
          <Menu.Item key='pwd' onClick={()=>{updatePwdFunc()}}>
            <span>修改密码</span>
          </Menu.Item>
          <Menu.Item onClick={()=>{logoutFunc()}} key='logout'>
            <span>退出登录</span>
          </Menu.Item>
        </Menu>
      );

      function confirm() {
        Modal.confirm({
          title: '退出登录',
          icon: <ExclamationCircleOutlined />,
          content: '是否退出登录',
          okText: '确认',
          cancelText: '取消',
          onOk() {
            removeToken()
            props.history.push('/login')
          },
          onCancel() {
            console.log('Cancel');
          },
        });
      }
    const rootSubmenuKeys = menuList
    const [openKeys, setOpenKeys] =useState([pathKey]);

    const onOpenChange = keys => {
        const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
        if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
        setOpenKeys(keys);
        } else {
        setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
        }
    };

    //左边菜单栏
    function renderItem(menus){
		return menus.map((item)=>{
			if (item.children) {
				 return (
				 	<SubMenu key={item.path} icon={<item.icon />} title={ item.title }>
				 	 	{ renderItem(item.children) }
				 	 </SubMenu>
				 	 )
			}else{
				return (
					<Menu.Item key={item.path} icon={<item.icon/>} onClick={()=>{siderClick(item.path)}}>
						{item.title}
					</Menu.Item>
				)
			}
				
		})
	}
    
    //路由跳转
    function siderClick(value){
        props.history.push(value)
    }

    //退出登录
    function logoutFunc(){
        confirm()
    }

    //获取当前用户
    useEffect(()=>{
        userInfo().then(res=>{
            if(res.data.status===200){
                setUserInfo(res.data.userInfo)
            }
        })
    },[])
    //修改密码
    function updatePwdFunc(){
        updatePwd.current.editPwdModal(userInfoData)
    }
    return (
        <div className={styles.container}>
            <UpdateModal pwdRef={updatePwd} ></UpdateModal>
            <Layout>
                <Header className={styles.header}>
                {/* <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
                    <Menu.Item key="1">nav 1</Menu.Item>
                    <Menu.Item key="2">nav 2</Menu.Item>
                    <Menu.Item key="3">nav 3</Menu.Item>
                </Menu> */}
                <div className={styles['header-content']}>
                    <div className={styles['header-content-left']}>
                        <span>手机后台管理系统测试</span>
                    </div>
                    <div className={styles['header-content-right']}>
                    <Dropdown overlay={menu} placement="bottomCenter" arrow>
                        <Button ><CaretDownOutlined />{userInfoData.username}</Button>
                    </Dropdown>
                    </div>
                </div>
                </Header>
                <Layout style={{display:'flex'}}>
                    <Sider width={200} className="site-layout-background sider-style">
                        <Menu
                        mode="inline"
                        theme='dark'
                        selectedKeys={[pathname]}
                        defaultOpenKeys={[pathKey]}
                        openKeys={openKeys} 
                        onOpenChange={onOpenChange}
                        style={{borderRight: 0 }}
                        >
                            {/* <SubMenu key="sub1" icon={<UserOutlined />} title="subnav 1">
                                <Menu.Item key="1">option1</Menu.Item>
                                <Menu.Item key="2">option2</Menu.Item>
                                <Menu.Item key="3">option3</Menu.Item>
                                <Menu.Item key="4">option4</Menu.Item>
                            </SubMenu> */}
                            {renderItem(adminRouter)}

                            {
                                // adminRouter.map((item)=>{
                                //     return <Menu.Item 
                                //     key={item.path} 
                                //     onClick={()=>{siderClick(item.path)}}
                                //     >   
                                //         {/* <MyIcon  type={item.icon}></MyIcon> */}
                                //         {item.title}
                                //     </Menu.Item>
                                // })
                            }
                        </Menu>
                    </Sider>
                    <Layout style={{ padding: '10px 24px 24px'}}>
                        <Breadcrumb >
                            <Breadcrumb.Item>Dashbord</Breadcrumb.Item>
                            {firstTitle?(<Breadcrumb.Item>{firstTitle}</Breadcrumb.Item>):''}
                            {secondTitle?(<Breadcrumb.Item>{secondTitle}</Breadcrumb.Item>):''}
                        </Breadcrumb>
                        <Spin tip="Loading..." spinning={props.spinning} > 
                            <Content
                                className="site-layout-background content-style"
                                style={{marginTop:'10px',height:'100%'}}
                                >
                                {props.children}
                            </Content>
                        </Spin>
                    </Layout>
                </Layout>
            </Layout>
        </div>
    )
}

export default connect(mapStateToProps,null)(withRouter(Frame))
