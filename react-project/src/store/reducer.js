/*
 * @Descripttion: reducer文件
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-09-05 16:43:49
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-12 16:13:32
 */

const initialUser ={
    userData:{},
    spinning:false
}

const userReducer = (state=initialUser, action)=>{
    action=action ||{type:''}
    switch(action.type){
        case 'USER_LOGIN':
        return {
            ...state,
            userData: action.data
        } ;
        case 'SPIN_SHOW':
            return {
                ...state,
                spinning:action.flag
            }
        case 'SPIN_HIDE':
            return {
                ...state,
                spinning:action.flag
            }
        default: return state
    }

}
export default userReducer
