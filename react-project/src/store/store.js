/*
 * @Descripttion: 创建store
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-09-05 16:43:54
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-12 15:40:46
 */
// import reducer from './reducer'
// import {createStore,applyMiddleware} from 'redux'
// import thunk from 'redux-thunk'
// const middleware=applyMiddleware(thunk)
// const store =createStore(reducer,middleware)
// export default store

// import {createStore, compose, applyMiddleware } from 'redux';
// import reducer from './reducer';
// import thunk from 'redux-thunk';
// // persist store
// import {persistStore, persistReducer} from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
// const myPersistReducer = persistReducer({
// key: 'root',
// storage: storage,
// whitelist: ['userData']
// }, reducer)
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// const store = createStore(myPersistReducer,composeEnhancers(
// applyMiddleware(thunk),
// ))
// export const persistor = persistStore(store)
// export default store

import { createStore, applyMiddleware, compose } from 'redux';
import reducer from './reducer';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
// 存储对象，默认存储到localstorage
const persistConfig = {
  key: 'root',
  storage,
}
// 重新组装reducer
const persistedReducer = persistReducer(persistConfig, reducer)
// redux调试工具配置代码
const composeEnhancers =
  typeof window === 'object' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?   
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

const enhancer = composeEnhancers(
  applyMiddleware(thunk),
);
// 创建store
const store = createStore(persistedReducer, enhancer);
// 应用redux-persist以完成数据持久化
export const persistor = persistStore(store)

export default store;



