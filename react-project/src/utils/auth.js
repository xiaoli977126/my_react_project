/*
 * @Descripttion: 获取后台token
 * @version: 1.0
 * @Author: 李国盛
 * @Date: 2021-08-04 10:20:21
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-07 20:39:09
 */

//获取token
import Cookie from 'js-cookie'
function getToken(){
    return Cookie.get('tokenKey')
}

//设置token
function setToken(token){
    return Cookie.set('tokenKey',token)
}

//移除token
function removeToken(){
    return Cookie.remove('tokenKey')
}

//判断用户是否登录
function userLogined(){
    if(getToken()){
        return true
    }
    return false
}

export {
    getToken,
    setToken,
    removeToken,
    userLogined
}