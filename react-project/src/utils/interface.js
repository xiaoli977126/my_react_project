/*
 * @Descripttion: your project
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-08-04 18:50:06
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-11 19:15:14
 */
import serve from './request'

//登录接口
const login=(obj)=>{
    return  serve.post('user/koa/api/login',obj)
}

//获取当前用户
const userInfo=()=>{
    return serve.post('user/koa/api/userInfo')
}
//添加或修改手机信息
const phoneAddOrSave=(data)=>{
    return serve.post('phone/koa/api/phoneAddOrSave',data)
}
//分页查询手机信息
const phoneQueryPages=(data)=>{
    return serve.post('phone/koa/api/phoneQueryPages',data)
}

//删除手机信息
const phoneDel=(data)=>{
    return serve.get('phone/koa/api/phoneDel',{params:data})
}

//修改密码
const editUserPwd=(data)=>{
    return serve.post('user/koa/api/editUserPwd',data)
}

//找回密码
const findpwd=(data)=>{
    return serve.get('user/koa/api/findpwd',{params:data})
}

//添加用户信息
const addOrSave=(data)=>{
    return serve.post('user/koa/api/addOrSave',data)
}
//分页查询获取用户信息
const queryPage=(data)=>{
    return serve.post('user/koa/api/queryPage',data)
}
//删除用户信息
const delUser=(data)=>{
    return serve.get('user/koa/api/delUser',{params:data})
}
export{
    login ,//用户登录
    phoneAddOrSave,//添加或修改手机信息
    phoneQueryPages,//手机分页查询
    phoneDel,//删除手机信息
    userInfo,//当前用户信息
    editUserPwd,//修改密码
    findpwd,//找回密码
    addOrSave,//添加用户信息
    queryPage,//分页查询获取用户信息
    delUser//删除用户信息
}