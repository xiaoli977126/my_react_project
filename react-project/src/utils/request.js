/*
 * @Descripttion: axios拦截器
 * @version: 1.0
 * @Author: 李国盛
 * @Date: 2021-08-04 10:44:53
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-05 23:24:53
 */

import axios from 'axios'
import {getToken} from '@/utils/auth'
import store from '../store/store'
let serve=axios.create({
    baseURL:'/',
    // headers:{
    //     'Content-type':'application/x-www-form-urlencoded'
    // }
})
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';//这个不受影响
// 请求request
serve.interceptors.request.use(
    (config)=>{
        const token=getToken()
        store.dispatch({type:'SPIN_SHOW',flag:true});
        if(token){
            config.headers.authorization=token
        }
        return config
    },
    (err)=>{
        return Promise.reject(err)
    }
)

// 响应response
serve.interceptors.response.use(
    res=>{
        store.dispatch({type:'SPIN_HIDE',flag:false});
        return res
    },
    err=>{
        return Promise.reject(err)
    }
)
export default serve

