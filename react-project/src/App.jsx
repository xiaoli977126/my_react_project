import React from 'react'
import Frame from './views/layout/Index'
import {adminRouter} from './routes/index'
// ,useParams,useRouteMatch
import {Switch,Route, Redirect,useLocation} from 'react-router-dom'
import {userLogined}from './utils/auth'
// import {userLogined} from './utils/auth'
function App(props) {
    // console.log(adminRouter)
    let {pathname} =useLocation()
    // let params=useParams()
    // let match=useRouteMatch()
    console.log('localtion:',pathname)
    // console.log('params:',params)
    // console.log('match:',match)
    let menuList=[]
    function renderRoutes(routes){
        return routes.map(item=>{
            menuList.push(item.path)
            localStorage.getItem('menuList',menuList)
            if(item.children){
                return (
                    item.children.map(items=>{
                        if(items.path===pathname){
                            console.log('items.path:',items.path)
                            document.title=items.title
                            // setstate(items.title)    
                            localStorage.setItem('firstTitle',item.title)
                            localStorage.setItem('secondTitle',items.title)
                            localStorage.setItem('pathKey',item.path)
                            
                        }
                        return (<Route key={items.path} {...items} exact></Route>)
                    })
                )
            }else{
                if(item.path===pathname){
                    document.title=item.title
                    localStorage.setItem('firstTitle',item.title)
                    localStorage.removeItem('secondTitle')
                }
                return (<Route key={item.path} {...item}></Route>)
            }
        })
    }
    return (
        userLogined()?(
            <Frame 
                firstTitle={localStorage.getItem('firstTitle')} 
                secondTitle={localStorage.getItem('secondTitle') }
                pathKey={localStorage.getItem('pathKey')} 
                menuList={menuList}
                
            >
                <Switch>
                    {renderRoutes(adminRouter)}
                    <Redirect from='/admin' to={adminRouter[0].children[0].path} exact></Redirect>
                    <Redirect to='/404'></Redirect>
                </Switch>
            </Frame>
        ):(
            <Redirect to='/login'></Redirect>
        )
    )
}

export default App
