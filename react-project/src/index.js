/*
 * @Descripttion: 入口文件
 * @version: 1.0
 * @Author: 李国盛
 * @Date: 2021-07-29 12:22:47
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-12 15:33:57
 */
import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter as Router,Route,Switch,Redirect,useLocation} from 'react-router-dom'
import './index.css';
import App from './App'
import {loginRouter,findPwdRouter} from './routes/index'
import { Provider } from "react-redux"
import store from "./store/store"
import { persistor } from "./store/store"
import { PersistGate } from 'redux-persist/lib/integration/react'
import zhCN from 'antd/lib/locale/zh_CN';
import { ConfigProvider } from 'antd';
// import NotFund from './views/notFound/Index'
const Main=(props)=>{
  console.log('index:',props)
  let {pathname}=useLocation()
  return (
        <Switch>
          <Route path='/admin'  component={App}></Route>
          {
            loginRouter.map((item)=>{
              if(pathname===item.path){
                document.title=item.title
              }
              return <Route key={item.path} {...item}></Route>
            })
          }
          {
            findPwdRouter.map((item)=>{
              if(pathname===item.path){
                document.title=item.title
              }
              return <Route key={item.path} {...item}></Route>
            })
          }
          <Redirect from='/' to='/admin' exact></Redirect>
          <Redirect to='/404'></Redirect>
        </Switch>
  )
}
ReactDOM.render(
  <ConfigProvider locale={zhCN}>
    <Provider store={store}>
      <PersistGate loading={null}  persistor={persistor}>
        <Router>
            <Route path='/' component={Main}></Route>
        </Router>,
      </PersistGate>
    </Provider>
  </ConfigProvider>,
  document.getElementById('root')
  
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
