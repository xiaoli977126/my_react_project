/*
 * @Descripttion: your project
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-09-05 19:51:41
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-24 21:37:58
 */
const Router=require('koa-router')
const router=new Router()
const User=require('../../model/userModel')
const jwt_decode=require('jwt-decode')
const path=require('path')
//用户登录
router.post('/login',async(ctx)=>{
    const jwt=require('jsonwebtoken')
    let secret=require('../../secret')
    //ctx.request.body
    //ctx.request.query
    //ctx.params
    const params=ctx.request.body
    const username=params.username
    const password=params.password
    const rolename=params.rolename
    // console.log(rolename)
    const user=await User.findOne({username})
    if(!user){
        ctx.body={
            msg:'该用户不存在',
            status:422,
            params
        }
    }else if(password!==user.password){
        ctx.body={
            msg:'密码不正确',
            status:422
        }
    }else if(rolename!==user.rolename){
        ctx.body={
            msg:'请选择正确的角色',
            status:422
        }
    }
    else{
        const token=jwt.sign({
            username:user.username,
            id:user._id,
            time:new Date().valueOf(),
            timeout:1000*60*60*3
        },secret,{expiresIn:'3h'})
        ctx.body={
            msg:'登陆成功',
            params:ctx.request.body,
            token,
            status:200
        }
        // console.log('decodeToken:',jwt.decode(token))
    }
})
//添加或修改用户
router.post('/addOrSave',async(ctx)=>{
    let username=ctx.request.body.username
    let password=ctx.request.body.password
    let telephone=ctx.request.body.telephone
    let email=ctx.request.body.email
    let rolename=ctx.request.body.rolename
    let imageUrl=ctx.request.body.imageUrl
    const id=ctx.request.body.id
    console.log(imageUrl)
    const user=await User.findOne({username})
    if(id){
        const result =await User.updateOne({_id:id},{username,telephone,password,email,rolename,imageUrl})
        if(result){
            ctx.body={
                msg:'操作成功',
                status:200
            }
        }
    }else{
        if(user){
            ctx.body={
                msg:'该用户已存在',
                status:422
            }
        }else{
            const result=await User.create({username,password,telephone,email,rolename,imageUrl})
            if(result){
                ctx.body={
                    msg:'操作成功',
                    list:result,
                    status:200
                }
            }
        }
    }
    
})

//分页查询获取用户信息
router.post('/queryPage',async(ctx)=>{
    const current=ctx.request.body.current
    const pageSize=ctx.request.body.pageSize
    const result=await User.find().skip((current-1)*pageSize).limit(pageSize)
    const totalResult=await User.find()
    if(result&&totalResult){
        ctx.body={
            current,
            pageSize,
            list:result,
            total:totalResult.length,
            msg:'查询成功',
            status:200
        }
    }
})
//获取当前用户
router.post('/userInfo',async(ctx)=>{
    const token=ctx.request.headers.authorization;
    if(token){
        playLoad=jwt_decode(token)
        ctx.body={
            userInfo:playLoad,
            msg:'请求成功',
            status:200
        }
    }else{
        ctx.body={
            msg:'改token已失效',
            status:422
        }
    }
})
//修改密码
router.post('/editUserPwd',async(ctx)=>{
    const oldPwd=ctx.request.body.oldPwd;
    const newPwd=ctx.request.body.newPwd;
    const id=ctx.request.body.id
    const user=await User.findOne({_id:id})
    if(oldPwd!==user.password){
        ctx.body={
            msg:'旧密码不正确',
            status:422
        }
    }else{
        const result =await User.updateOne({_id:id},{password:newPwd})
        if(result){
            ctx.body={
                msg:'更改成功,请重新登录',
                status:200
            }
        }
    }
})
//找回密码
router.get('/findpwd',async(ctx)=>{
    const str=ctx.request.query.emailAndphone
    let telephoneResult=await User.findOne({telephone:str})
    let emailResult=await User.findOne({email:str})
    if(telephoneResult){
        ctx.body={
            msg:'密码找回成功',
            status:200,
            password:telephoneResult.password
        }
    }else if(emailResult){
        ctx.body={
            msg:'密码找回成功',
            status:200,
            password:emailResult.password
        }
    }else{
        ctx.body={
            msg:'密码找回失败，请输入正确的手机号码或邮箱',
            status:422,
        }
    }
})
//删除用户信息
router.get('/delUser',async(ctx)=>{
    const id=ctx.request.query.id
    const result =await User.deleteOne({_id:id})
    if(result){
        ctx.body={
            msg:'删除成功',
            status:200
        }
    }else{
        ctx.body={
            msg:'删除失败',
            status:200
        }
    }
})

//首先是封装upload.js代码部分
const multer = require('koa-multer')
const destPath=path.join(__dirname,"../../uploads");
//配置
var storage = multer.diskStorage({
    //文件保存路径
    destination: function (req, file, cb) {
        cb(null, destPath)
    },
    //修改文件名称
    filename: function (req, file, cb) {
        var fileFormat = (file.originalname).split(".");  //以点分割成数组，数组的最后一项就是后缀名
        cb(null, Date.now() + "." + fileFormat[fileFormat.length - 1]);
    }
})
//加载配置
var upload = multer({
     storage: storage,
     limits: {
        fileSize: 1024*1024*10 
      }
});
//koa后台监听post请求，将图片上传后生成链接返回前端
router.post('/upload', upload.single('file'), async (ctx, next) => {
    ctx.body = {
        filename: ctx.req.file.filename
    }
})
module.exports=router.routes()