/*
 * @Descripttion: your project
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-09-07 20:27:12
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-07 23:39:52
 */
const Phone=require('../../model/phoneModel/index')
const Router=require('koa-router')
const router=new Router()
const auth=require('../../tokenVerify/index')
//添加或修改手机
router.post('/phoneAddOrSave',async(ctx)=>{
    const id=ctx.request.body.id
    const name=ctx.request.body.name
    const price=ctx.request.body.price
    const phone=await Phone.findOne({name})
    if(id){
         result=await Phone.updateOne({_id:id},{name,price})
         if(result){
            ctx.body={
                msg:'操作成功',
                status:200
            }
        }
    }else{
        if(phone){
            ctx.body={
                msg:'该手机名称已存在',
                status:422
            }
        }else{
            result=await Phone.create({name,price})
            if(result){
                ctx.body={
                    msg:'操作成功',
                    status:200
                }
            }
        }
    }
})

//分页查询手机
router.post('/phoneQueryPages',async(ctx)=>{
    const current=ctx.request.body.current
    const pageSize=ctx.request.body.pageSize
    const result=await Phone.find().skip((current-1)*pageSize).limit(pageSize)
    const totalResult=await Phone.find()
    if(result&&totalResult){
        ctx.body={
            current,
            pageSize,
            list:result,
            total:totalResult.length,
            msg:'查询成功',
            status:200
        }
    }
})

router.get('/phoneDel',async(ctx)=>{
    const _id=ctx.request.query.id
    const result=await Phone.deleteOne({_id})
    if(result){
        ctx.body={
            msg:'删除成功',
            status:200,
        }
    }
})
module.exports=router.routes()
