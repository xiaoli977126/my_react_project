const mongoose=require('mongoose')
mongoose.connect('mongodb://localhost/reactKoa',{
    useNewUrlParser:true,
    useUnifiedTopology:true
})

const phoneSchema=new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    price:{
        type:String,
        required:true
    }
})
const Phone=mongoose.model('koaPhone',phoneSchema)
module.exports=Phone