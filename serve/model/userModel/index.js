/*
 * @Descripttion: your project
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-09-05 19:41:38
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-24 21:40:14
 */
const mongoose=require('mongoose')
mongoose.connect('mongodb://localhost/reactKoa',{
    useNewUrlParser:true,
    useUnifiedTopology:true,
})

const userSchema=new mongoose.Schema({
    username:{
        required:true,
        type:String,
        default:''
    },
    password:{
        type:String,
        required:true,
        default:''
    },
    email:{
        type:String,
        required:true,
        default:''
    },
    telephone:{
        type:String,
        required:true,
        default:''
    },
    rolename:{
        type:String,
        required:true,
        default:''
    },
    imageUrl:{
        type:String,
        required:true,
        default:''
    }
})

const User =mongoose.model('koaUsers',userSchema)
module.exports=User