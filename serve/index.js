/*
 * @Descripttion: your project
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-08-06 20:12:22
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-22 22:51:14
 */
const Koa =require('koa')
const Router=require('koa-router')
const app=new Koa()
const router=new Router()
// const path = require('path');
const user =require('./router/userRouter/index')
const phone =require('./router/phoneRouter/index')
const bodyParser=require('koa-bodyparser')
const tokenCheck=require('./tokenVerify/index')
// const static = require('koa-static');
// const { historyApiFallback } = require('koa2-connect-history-api-fallback');
app.use(require('@koa/cors')())
// app.use(historyApiFallback({
//     index: '/index.html'
// })).use(bodyParser()).use(static(path.join( __dirname, '/public')))
app.use(bodyParser())
router.use('/user/koa/api',user)
router.use('/phone/koa/api',phone)

// app.use(tokenCheck)
app.use(router.routes()).use(router.allowedMethods())
app.listen(8080,()=>{
    console.log('8080端口启动成功')
})