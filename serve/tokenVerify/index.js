/*
 * @Descripttion: your project
 * @version: 1.0
 * @Author: Aidam_Bo
 * @Date: 2021-09-07 23:26:35
 * @LastEditors: Aidam_Bo
 * @LastEditTime: 2021-09-21 13:25:37
 */
const Promise = require("bluebird");
const jwt = require("jsonwebtoken");
const verify = Promise.promisify(jwt.verify);
let secret  = require("../secret/index");

async function check(ctx, next) {
  let url = ctx.request.url;
  console.log(ctx.request)
  // 登录 不用检查
  if (url.indexOf("/user/koa/api/login")!==-1||url.indexOf("/user/koa/api/findpwd")!==-1) await next();
  else {
      // 规定token写在header 的 'autohrization' 
    let token = ctx.request.headers["authorization"];
    // 解码
    let payload = await verify(token,secret);
    // console.log('payload',payload)
    let { time, timeout } = payload;
    let data = new Date().getTime();
    if (data - time <= timeout) {
        // 未过期
      await next();
    } else {
        //过期
      ctx.body = {
        status: 422,
        message:'token 已过期'
      };
    }
  }
}

module.exports = check
